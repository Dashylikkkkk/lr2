const { Sequelize } = require("sequelize");
require('dotenv').config();

const POSTGRES_HOST = Number(process.env.POSTGRES_HOST) || '127.0.0.1';
const POSTGRES_USERNAME = Number(process.env.POSTGRES_USERNAME) || 'test';
const POSTGRES_PASSWORD = Number(process.env.POSTGRES_PASSWORD) || '123';
const POSTGRES_PORT = Number(process.env.POSTGRES_PORT) || '5432'


const sequelize = new Sequelize({
  host: POSTGRES_HOST,
  dialect: "postgres",
  database: "test",
  username: POSTGRES_USERNAME,
  password: POSTGRES_PASSWORD,
  port: POSTGRES_PORT,
});

async function initDB() {
  try {
    await sequelize.authenticate();
    await sequelize.dropSchema('public', {});
    await sequelize.createSchema('public', {});
    await sequelize.sync();
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
}

module.exports = {
  initDB,
  sequelize,
};
