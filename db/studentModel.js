const { sequelize } = require("./dbconfig");
const { Sequelize } = require("sequelize");
const Record = require("./recordsModel");

class Student extends Sequelize.Model {}

Student.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    surname: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    dateOfBirth: {
      type: Sequelize.DATE,
      allowNull: false,
    },
  },
  { sequelize: sequelize, modelName: "student" }
);

Student.hasMany(Record);

Record.belongsTo(Student, {
  foreignKey: "studentId",
});

module.exports = Student;