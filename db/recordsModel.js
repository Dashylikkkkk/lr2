const { sequelize } = require("./dbconfig");
const { Sequelize } = require("sequelize");

class Record extends Sequelize.Model {}

Record.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV4,
      primaryKey: true,
    },
    subject: {
      type: Sequelize.DataTypes.STRING,
      allowNull: false,
    },
    grade: {
      type: Sequelize.DataTypes.INTEGER,
      allowNull: false,
    },
    teacherFullName: {
      type: Sequelize.DataTypes.STRING,
      allowNull: false,
    }
  },
  { sequelize: sequelize, modelName: "record" }
);

module.exports = Record;
