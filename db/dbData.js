const Record = require("./recordsModel");
const Student = require("./studentModel");

async function insertDb(){
    const student1 = await Student.create({
        name: 'Иван',
        surname: 'Иванов',
        dateOfBirth: new Date('2001-09-18'),
    })

    const student2 = await Student.create({
        name: 'Максим',
        surname: 'Максимов',
        dateOfBirth: new Date('2002-09-18'),
    })

    const student3 = await Student.create({
        name: 'Ибрагим',
        surname: 'Ибрагимов',
        dateOfBirth: new Date('2002-10-18'),
    })

   const students = [student1,student2,student3];
   const subjects = ['Мат. анализ', 'Лин. алгебра', 'Информатика'];
   const teacherFullNames = ['Вязников Иван Валерьевич','Костюшов Алексей Игоревич','Костомаха Иван Евгеньевич'];

    for (let i=0; i<3; i++){
        for (let j=0; j<3; j++){
            await Record.create({
                subject: subjects[j],
                teacherFullName: teacherFullNames[j],
                studentId: students[i].id,
                grade: (Math.floor(Math.random() * 4) + 1)
            })
        }
    }
}

module.exports = {
    insertDb
}