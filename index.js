const { initDB } = require("./db/dbconfig");
const { insertDb } = require("./db/dbData");
const Record = require("./db/recordsModel");
const Student = require("./db/studentModel");
const fs = require('fs');

async function start(){
    await initDB();
    await insertDb();
    const studentList = await Student.findAll({
        attributes: ['name','surname'],
        include:[
            {
                model: Record,
                attributes: ['grade','subject']
            }
        ]
    })


    let bestStudent = {};
    let bestAvg = 0;
    for (const student of studentList){
        gradeAvg = 0;
        for (let i=0;i<student.records.length;i++){
            gradeAvg+= (student.records[i].grade/student.records.length);
        }
        if (gradeAvg > bestAvg){
            bestAvg = gradeAvg;
            bestStudent = student;
        }
    }
    
    console.log(bestStudent.toJSON());
    fs.writeFileSync('output/result.json',JSON.stringify(bestStudent))
}

start();